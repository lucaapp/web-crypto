import { z as zod } from 'zod';
import validator from 'validator';
import { base64ToBytes } from './encoding';

export const z = {
  ...zod,
  uuid: () =>
    zod
      .string()
      .length(36)
      .refine(value => validator.isUUID(value, 'all')),
  base64: ({
    min,
    max,
    length,
    rawLength,
  }: {
    min?: number;
    max?: number;
    length?: number;
    rawLength?: number;
  } = {}) =>
    zod
      .string()
      .min(min as number)
      .max(max as number)
      .length(length as number)
      .refine(value => {
        if (!validator.isBase64(value)) return false;
        if (!rawLength) return true;

        return base64ToBytes(value).length === rawLength;
      }),
  unixTimestamp: () => zod.number().int().positive(),
  ecPublicKey: () => z.base64({ length: 88, rawLength: 65 }),
  iv: () => z.base64({ length: 24, rawLength: 16 }),
  mac: () => z.base64({ length: 44, rawLength: 32 }),
  keyId: () => z.number().int().min(0),
};
