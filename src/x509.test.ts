import {
  ecPublicKeyToPEM,
  ecPrivateKeyToPEM,
  getCommonName,
  getOrganizationalUnitName,
} from './x509';
import { createTestCertificate } from './entities/testHelper';

describe('pem', () => {
  describe('privateKeyToECPrivateKeyPEM', () => {
    const privateKey =
      '042ec344e962b448ed7eda3e203096c7995b7bb15402145c33406119406efbf3dcc7ce5884d0a88581d0a00d710dfd6fb2c183b299dfefdea402e9c82171396b89';
    describe('when privateKeyToECPrivateKeyPEM called with a private key', () => {
      it('return correct pem file content', () => {
        expect(ecPrivateKeyToPEM(privateKey))
          .toEqual(`-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIAQuw0TpYrRI7X7aPiAwlseZW3uxVAIUXDNAYRlAbvvz3MfOWITQqIWB0KANcQ39b7LBg7KZ3+/epALpyCFxOWuJoAoGCCqGSM49AwEHoUQDQgAEv/D3mUEwNB8bKORLhHplBryaRrhBLbDHU8U2L7bP0+99QZ7AQq71lrWCB1Z84SKTEL0YL6driVQuqSamfHP3Mw==
-----END EC PRIVATE KEY-----`);
      });
    });
  });
  describe('publicKeyToECPublicKeyPEM', () => {
    const publicKey =
      '04ef1fbfd272a33425c2282b67ec5171e096e8cd6a3721370206109ba1a3bd67de044572063699f39e193e527e591c42af8036dd040a0bd1154186caf2867cb232';
    describe('when publicKeyToECPublicKeyPEM called with a public key', () => {
      it('return correct pem file content', () => {
        expect(ecPublicKeyToPEM(publicKey)).toEqual(`-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE7x+/0nKjNCXCKCtn7FFx4JbozWo3ITcCBhCboaO9Z94ERXIGNpnznhk+Un5ZHEKvgDbdBAoL0RVBhsryhnyyMg==
-----END PUBLIC KEY-----`);
      });
    });
  });
  describe('getCommonName', () => {
    it('returns the common name', () => {
      const cert = createTestCertificate({
        commonName: 'Test Name',
      });
      expect(getCommonName(cert.cert)).toEqual('Test Name');
    });
  });
  describe('getOrganizationUnitName', () => {
    it('returns the organization unit', () => {
      const cert = createTestCertificate({
        organizationalUnitName: 'Test Unit',
      });
      expect(getOrganizationalUnitName(cert.cert)).toEqual('Test Unit');
    });
  });
});
