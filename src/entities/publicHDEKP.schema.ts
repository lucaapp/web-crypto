import { z } from '../zod';

export const signedPublicHDEKPSchema = z
  .object({
    type: z.literal('publicHDEKP'),
    iat: z.unixTimestamp(),
    iss: z.string().length(40),
    sub: z.uuid(),
    name: z.string().max(255),
    key: z.ecPublicKey(),
  })
  .strict();
