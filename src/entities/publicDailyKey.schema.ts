import { z } from '../zod';

export const signedPublicDailyKeySchema = z
  .object({
    type: z.literal('publicDailyKey'),
    iat: z.unixTimestamp(),
    iss: z.uuid(),
    keyId: z.keyId(),
    key: z.ecPublicKey(),
    exp: z.unixTimestamp().optional(),
  })
  .strict();
