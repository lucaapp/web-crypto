import { JsonWebTokenError, TokenExpiredError } from 'jsonwebtoken';

import {
  CertificateChainInvalidError,
  CommonNameMismatchError,
} from '../errors';
import { getFingerprint } from '../x509';
import {
  createSignedPublicHDSKP,
  verifySignedPublicHDSKP,
} from './publicHDSKP';

import { hexToBase64 } from '../encoding';
import { createIssuer, createTestCertificate } from './testHelper';

const now = Math.floor(Date.now() / 1000);

describe('Crypto / Entities / publicHDSKP', () => {
  beforeEach(() => {
    jest.useFakeTimers('modern');
    jest.setSystemTime(now * 1000);
  });
  afterEach(() => {
    jest.useRealTimers();
  });

  describe('createSignedPublicHDSKP', () => {
    describe('when everything is correct', () => {
      it('creates a valid jwt', () => {
        const { issuer, hdskp, certificateChain, issuerCert } = createIssuer(
          {}
        );

        const signedPublicHDSKP = createSignedPublicHDSKP({
          issuer,
          certPrivateKey: issuerCert.privateKey,
          publicHDSKP: hdskp.publicKey,
        });

        expect(signedPublicHDSKP).toEqual(issuer.signedPublicHDSKP);

        const content = verifySignedPublicHDSKP({
          certificateChain,
          issuer: { ...issuer, signedPublicHDSKP },
        });

        expect(content).toEqual({
          iat: now,
          sub: issuer.issuerId,
          iss: getFingerprint(issuerCert.cert),
          name: issuer.name,
          key: hexToBase64(hdskp.publicKey),
          type: 'publicHDSKP',
        });
      });
      it('sets the correct issue time', () => {
        const { issuer, hdskp, certificateChain, issuerCert } = createIssuer(
          {}
        );
        const iat = 1234;

        const signedPublicHDSKP = createSignedPublicHDSKP({
          issuer,
          certPrivateKey: issuerCert.privateKey,
          publicHDSKP: hdskp.publicKey,
          iat,
        });

        const content = verifySignedPublicHDSKP({
          certificateChain,
          issuer: { ...issuer, signedPublicHDSKP },
        });

        expect(content).toEqual({
          iat,
          sub: issuer.issuerId,
          iss: getFingerprint(issuerCert.cert),
          name: issuer.name,
          key: hexToBase64(hdskp.publicKey),
          type: 'publicHDSKP',
        });
      });
    });
  });

  describe('verifySignedPublicHDSKP', () => {
    describe('when everything is correct', () => {
      it('extracts the jwts content', () => {
        const { issuer, hdskp, certificateChain, issuerCert } = createIssuer(
          {}
        );
        const content = verifySignedPublicHDSKP({
          certificateChain,
          issuer,
        });

        expect(content).toEqual({
          iat: now,
          sub: issuer.issuerId,
          iss: getFingerprint(issuerCert.cert),
          name: issuer.name,
          key: hexToBase64(hdskp.publicKey),
          type: 'publicHDSKP',
        });
      });
    });
    describe('when certificate expired', () => {
      it('throws an CertificateChainInvalidError', () => {
        const { issuer, certificateChain } = createIssuer({});
        jest.setSystemTime(now * 2000);
        expect(() =>
          verifySignedPublicHDSKP({
            certificateChain,
            issuer,
          })
        ).toThrow(CertificateChainInvalidError);
      });
    });
    describe('when certificate chain is invalid', () => {
      it('throws an CertificateChainInvalidError', () => {
        const { issuer, certificateChain } = createIssuer({});
        const { issuerCert: wrongIssuerCert } = createIssuer({});

        expect(() =>
          verifySignedPublicHDSKP({
            certificateChain: [certificateChain[0], wrongIssuerCert.cert],
            issuer,
          })
        ).toThrow(CertificateChainInvalidError);
      });
    });

    describe('when the issuer uuid is incorrect', () => {
      it('throws an JsonWebTokenError', () => {
        const { issuer, certificateChain } = createIssuer({});
        const invalidUuid = '304fdc8f-526f-4df7-abcf-000000000000';
        expect(() =>
          verifySignedPublicHDSKP({
            certificateChain,
            issuer: { ...issuer, issuerId: invalidUuid },
          })
        ).toThrow(JsonWebTokenError);
      });
    });

    describe('when isIssuing and the jwt is too old ', () => {
      it('throws an TokenExpiredError', () => {
        const { issuer, certificateChain } = createIssuer({});
        jest.setSystemTime((now + 60 * 11) * 1000);
        expect(() =>
          verifySignedPublicHDSKP({
            certificateChain,
            issuer,
            isIssuing: true,
          })
        ).toThrow(TokenExpiredError);
      });
    });
  });
});
