import * as jwt from 'jsonwebtoken';
import { z } from 'zod';
import { Hex, PEM, Issuer } from '../types';
import { base64ToHex } from '../encoding';
import { ecPublicKeyToPEM, ecPrivateKeyToPEM } from '../x509';
import { verifyJWT } from '../jwt';

import { signedLocationTransferSchema } from './locationTransfer.schema';
import { verifySignedPublicHDSKP } from './publicHDSKP';

/**
 * Create signed location transfer token
 * @param {Issuer} issuer Issuer creating the token
 * @param {PEM} privateHDSKP private key to sign token with
 * @param {string} locationId uuid of the location requested
 * @param {number[]} time time range of the request
 * @returns {string} token
 */
export const createSignedLocationTransfer = ({
  issuer,
  privateHDSKP,
  locationId,
  time,
  iat,
}: {
  issuer: Issuer;
  privateHDSKP: Hex;
  locationId: string;
  time: [number, number];
  iat?: number;
}) => {
  return jwt.sign(
    {
      type: 'locationTransfer',
      iss: issuer.issuerId,
      locationId,
      time,
      ...(iat && { iat }),
    },
    ecPrivateKeyToPEM(privateHDSKP),
    { algorithm: 'ES256' }
  );
};

/**
 * Verify signed location transfer token
 * @param {PEM[]} certificateChain certificate chain to validate token against
 * @param {Issuer} issuer issuer who created the token
 * @param {string} token token to verify
 * @param {boolean} isIssuing indicate if the issue time should be verified
 * @returns {SignedLocationTransfer} validated token content
 */
export function verifySignedLocationTransfer({
  certificateChain,
  issuer,
  signedLocationTransfer,
  isIssuing = false,
}: {
  certificateChain: PEM[];
  issuer: Issuer;
  signedLocationTransfer: string;
  isIssuing?: boolean;
}): z.infer<typeof signedLocationTransferSchema> {
  const hdskp = verifySignedPublicHDSKP({
    certificateChain,
    issuer,
  });

  return verifyJWT<z.infer<typeof signedLocationTransferSchema>>({
    schema: signedLocationTransferSchema,
    algorithm: 'ES256',
    token: signedLocationTransfer,
    publicKeyPem: ecPublicKeyToPEM(base64ToHex(hdskp.key)),
    issuer: issuer.issuerId,
    isIssuing,
  });
}
