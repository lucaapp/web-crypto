import * as jwt from 'jsonwebtoken';
import { z } from 'zod';
import { Hex, Base64, PEM, Issuer } from '../types';
import { base64ToHex } from '../encoding';
import { ecPublicKeyToPEM, ecPrivateKeyToPEM } from '../x509';
import { verifyJWT } from '../jwt';

import { signedPublicDailyKeySchema } from './publicDailyKey.schema';
import { verifySignedPublicHDSKP } from './publicHDSKP';

/**
 * Create signed public daily key token
 * @param {Issuer} issuer Issuer creating the token
 * @param {PEM} privateHDSKP private key to sign token with
 * @param {number} keyId id of the daily key
 * @param {Base64} key public daily key
 * @returns {string} token
 */
export const createSignedPublicDailyKey = ({
  issuer,
  privateHDSKP,
  keyId,
  key,
  iat,
  exp,
  expiresIn,
}: {
  issuer: Issuer;
  privateHDSKP: Hex;
  keyId: number;
  key: Base64;
  iat?: number;
  exp?: number;
  expiresIn?: string | number;
}) => {
  return jwt.sign(
    {
      type: 'publicDailyKey',
      iss: issuer.issuerId,
      keyId,
      key,
      ...(exp && { exp }),
      ...(iat && { iat }),
    },
    ecPrivateKeyToPEM(privateHDSKP),
    { algorithm: 'ES256', ...(expiresIn && { expiresIn }) }
  );
};

/**
 * Verify signed public daily key token
 * @param {PEM[]} certificateChain certificate chain to validate token against
 * @param {Issuer} issuer issuer who created the token
 * @param {string} token token to verify
 * @param {boolean} isIssuing indicate if the issue time should be verified
 * @returns {SignedPublicDailyKey} validated token content
 */
export function verifySignedPublicDailyKey({
  certificateChain,
  issuer,
  signedPublicDailyKey,
  isIssuing = false,
}: {
  certificateChain: PEM[];
  issuer: Issuer;
  signedPublicDailyKey: string;
  isIssuing?: boolean;
}): z.infer<typeof signedPublicDailyKeySchema> {
  const hdskp = verifySignedPublicHDSKP({
    certificateChain,
    issuer,
  });

  return verifyJWT<z.infer<typeof signedPublicDailyKeySchema>>({
    schema: signedPublicDailyKeySchema,
    algorithm: 'ES256',
    token: signedPublicDailyKey,
    publicKeyPem: ecPublicKeyToPEM(base64ToHex(hdskp.key)),
    issuer: issuer.issuerId,
    isIssuing,
  });
}
