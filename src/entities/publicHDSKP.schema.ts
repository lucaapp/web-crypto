import { z } from '../zod';

export const signedPublicHDSKPSchema = z
  .object({
    type: z.literal('publicHDSKP'),
    iat: z.unixTimestamp(),
    iss: z.string().length(40),
    sub: z.uuid(),
    name: z.string().max(255),
    key: z.ecPublicKey(),
  })
  .strict();
