import { z } from '../zod';

export const signedEncryptedPrivateDailyKeySchema = z
  .object({
    type: z.literal('encryptedPrivateDailyKey'),
    iat: z.unixTimestamp(),
    iss: z.uuid(),
    sub: z.uuid(),
    key: z
      .object({
        data: z.base64({ rawLength: 32 }),
        iv: z.iv(),
        mac: z.mac(),
        publicKey: z.ecPublicKey(),
      })
      .strict(),
    keyId: z.keyId(),
    exp: z.unixTimestamp().optional(),
  })
  .strict();
