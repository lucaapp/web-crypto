import * as jwt from 'jsonwebtoken';
import { z } from 'zod';
import { Hex, Base64, PEM, Issuer } from '../types';
import { base64ToHex } from '../encoding';
import { ecPrivateKeyToPEM, ecPublicKeyToPEM } from '../x509';
import { verifyJWT } from '../jwt';

import { signedEncryptedPrivateBadgeKeySchema } from './encryptedPrivateBadgeKey.schema';
import { verifySignedPublicHDSKP } from './publicHDSKP';

/**
 * Create signed encrypted private badge key token
 * @param {Issuer} issuer Issuer creating the token
 * @param {PEM} privateHDSKP private key to sign token with
 * @param {string} subject uuid of the receiver of the token
 * @param {number} keyId id of the badge key
 * @param {EncryptedData} key encrypted private badge key
 * @returns {string} token
 */
export const createSignedEncryptedPrivateBadgeKey = ({
  issuer,
  privateHDSKP,
  subject,
  keyId,
  key,
  iat,
  exp,
  expiresIn,
}: {
  issuer: Issuer;
  privateHDSKP: Hex;
  subject: string;
  keyId: number;
  key: {
    data: Base64;
    iv: Base64;
    mac: Base64;
    publicKey: Base64;
  };
  iat?: number;
  exp?: number;
  expiresIn?: string | number;
}) => {
  return jwt.sign(
    {
      type: 'encryptedPrivateBadgeKey',
      iss: issuer.issuerId,
      sub: subject,
      keyId,
      key,
      ...(exp && { exp }),
      ...(iat && { iat }),
    },
    ecPrivateKeyToPEM(privateHDSKP),
    { algorithm: 'ES256', ...(expiresIn && { expiresIn }) }
  );
};

/**
 * Verify signed encrypted private badge key token
 * @param {PEM[]} certificateChain certificate chain to validate token against
 * @param {Issuer} issuer issuer who created the token
 * @param {string} token token to verify
 * @param {boolean} isIssuing indicate if the issue time should be verified
 * @returns {SignedEncryptedPrivateBadgeKey} validated token content
 */
export function verifySignedEncryptedPrivateBadgeKey({
  certificateChain,
  issuer,
  signedEncryptedPrivateBadgeKey,
  isIssuing = false,
}: {
  certificateChain: PEM[];
  issuer: Issuer;
  signedEncryptedPrivateBadgeKey: string;
  isIssuing?: boolean;
}): z.infer<typeof signedEncryptedPrivateBadgeKeySchema> {
  const hdskp = verifySignedPublicHDSKP({
    certificateChain,
    issuer,
  });

  return verifyJWT<z.infer<typeof signedEncryptedPrivateBadgeKeySchema>>({
    schema: signedEncryptedPrivateBadgeKeySchema,
    algorithm: 'ES256',
    token: signedEncryptedPrivateBadgeKey,
    publicKeyPem: ecPublicKeyToPEM(base64ToHex(hdskp.key)),
    issuer: issuer.issuerId,
    isIssuing,
  });
}
