import { v4 as uuidv4 } from 'uuid';
import * as forge from 'node-forge';
import { EC_KEYPAIR_GENERATE } from '../crypto';
import { ECKeyPair, Issuer, PEM } from '../types';
import { createSignedPublicHDSKP } from './publicHDSKP';
import { createSignedPublicHDEKP } from './publicHDEKP';

const CERT_CHAIN_COMMON_NAME = 'Dev CN';

interface TestCertificate {
  cert: PEM;
  privateKey: PEM;
  publicKey: PEM;
}

export const createTestCertificate = ({
  commonName,
  organizationalUnitName,
  notBefore,
  notAfter,
  signaturePrivateKey,
}: {
  commonName?: string;
  organizationalUnitName?: string;
  notBefore?: Date;
  notAfter?: Date;
  signaturePrivateKey?: forge.pki.PrivateKey;
}): TestCertificate => {
  const keys = forge.pki.rsa.generateKeyPair(2048);
  const cert = forge.pki.createCertificate();
  cert.publicKey = keys.publicKey;

  cert.validity.notBefore = notBefore || new Date();

  if (notAfter) {
    cert.validity.notAfter = notAfter;
  } else {
    cert.validity.notAfter = new Date();
    cert.validity.notAfter.setFullYear(
      cert.validity.notBefore.getFullYear() + 1
    );
  }

  const attrs = [
    {
      name: 'commonName',
      value: commonName || 'Test Certificate',
    },
  ];

  if (organizationalUnitName) {
    attrs.push({
      name: 'organizationalUnitName',
      value: organizationalUnitName,
    });
  }

  cert.setSubject(attrs);
  cert.setIssuer(attrs);

  cert.sign(signaturePrivateKey || keys.privateKey);

  return {
    cert: forge.pki.certificateToPem(cert),
    privateKey: forge.pki.privateKeyToPem(keys.privateKey),
    publicKey: forge.pki.publicKeyToPem(keys.publicKey),
  };
};

const createCertificateChainCert = ({
  signaturePrivateKey,
}: {
  signaturePrivateKey?: forge.pki.PrivateKey;
}) => {
  const keys = forge.pki.rsa.generateKeyPair(2048);
  const cert = forge.pki.createCertificate();
  cert.publicKey = keys.publicKey;

  cert.validity.notBefore = new Date();

  cert.validity.notAfter = new Date();
  cert.validity.notAfter.setFullYear(cert.validity.notBefore.getFullYear() + 1);

  const attrs = [
    {
      name: 'commonName',
      value: CERT_CHAIN_COMMON_NAME,
    },
  ];
  cert.setSubject(attrs);
  cert.setIssuer(attrs);

  cert.sign(signaturePrivateKey || keys.privateKey);

  return {
    cert: forge.pki.certificateToPem(cert),
    privateKey: keys.privateKey,
  };
};
interface CreateIssuerArgs {
  uuid?: string;
  commonName?: string;
}

export function createIssuer({
  uuid = uuidv4(),
  commonName = CERT_CHAIN_COMMON_NAME,
}: CreateIssuerArgs) {
  const caRoot = createCertificateChainCert({});
  const caBasic = createCertificateChainCert({
    signaturePrivateKey: caRoot.privateKey,
  });
  const issuerCert = createTestCertificate({
    commonName,
    signaturePrivateKey: caBasic.privateKey,
  });

  const baseIssuer = {
    issuerId: uuid,
    commonName,
    name: commonName,
    publicCertificate: issuerCert.cert,
  };

  const ecKeypair = EC_KEYPAIR_GENERATE();

  const signedPublicHDSKP = createSignedPublicHDSKP({
    issuer: baseIssuer,
    certPrivateKey: issuerCert.privateKey,
    publicHDSKP: ecKeypair.publicKey,
  });

  const signedPublicHDEKP = createSignedPublicHDEKP({
    issuer: baseIssuer,
    certPrivateKey: issuerCert.privateKey,
    publicHDEKP: ecKeypair.publicKey,
  });

  const issuer = {
    ...baseIssuer,
    signedPublicHDSKP,
    signedPublicHDEKP,
  };

  return {
    issuer,
    hdskp: ecKeypair,
    hdekp: ecKeypair,
    certificateChain: [caRoot.cert, caBasic.cert],
    issuerCert,
  };
}
