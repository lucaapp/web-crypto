import { z } from '../zod';

export const signedPublicBadgeKeySchema = z
  .object({
    type: z.literal('publicBadgeKey'),
    iat: z.unixTimestamp(),
    iss: z.uuid(),
    keyId: z.keyId(),
    key: z.ecPublicKey(),
    exp: z.unixTimestamp().optional(),
  })
  .strict();
