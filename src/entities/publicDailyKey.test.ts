import { sign, JsonWebTokenError, TokenExpiredError } from 'jsonwebtoken';
import { v4 as uuid } from 'uuid';
import { z } from 'zod';
import { ecPrivateKeyToPEM } from '../x509';
import {
  createSignedPublicDailyKey,
  verifySignedPublicDailyKey,
} from './publicDailyKey';
import { createIssuer } from './testHelper';

const now = Math.floor(Date.now() / 1000);
const keyId = 0;
const key =
  'BIgdz4CG78hBYu1NVyuTYTXbZryQ8F2sX+9InZRZAMI1W+lcSUlcOr+Zx6rGbJ3AHRHENQhoSNgqmWaAljCPnR4=';

describe('Crypto / Entities / publicDailyKey', () => {
  beforeEach(() => {
    jest.useFakeTimers('modern');
    jest.setSystemTime(now * 1000);
  });
  afterEach(() => {
    jest.useRealTimers();
  });
  describe('createSignedPublicDailyKey', () => {
    describe('when everything is correct', () => {
      it('creates a valid jwt', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedPublicDailyKey = createSignedPublicDailyKey({
          issuer,
          privateHDSKP: hdskp.privateKey,
          keyId,
          key,
        });
        const content = verifySignedPublicDailyKey({
          certificateChain,
          issuer,
          signedPublicDailyKey,
        });
        expect(content).toEqual({
          iat: now,
          type: 'publicDailyKey',
          iss: issuer.issuerId,
          keyId,
          key,
        });
      });
      it('sets the correct issue time', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const iat = 1234;
        const signedPublicDailyKey = createSignedPublicDailyKey({
          issuer,
          privateHDSKP: hdskp.privateKey,
          keyId,
          key,
          iat,
        });
        const content = verifySignedPublicDailyKey({
          certificateChain,
          issuer,
          signedPublicDailyKey,
        });
        expect(content).toEqual({
          iat,
          type: 'publicDailyKey',
          iss: issuer.issuerId,
          keyId,
          key,
        });
      });
    });
  });
  describe('verifySignedPublicDailyKey', () => {
    describe('when the algorithm is incorrect', () => {
      it('throws an JsonWebTokenError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedPublicDailyKey = sign(
          {
            type: 'publicDailyKey',
            iss: issuer.issuerId,
            keyId,
            key,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES512' }
        );
        expect(() =>
          verifySignedPublicDailyKey({
            certificateChain,
            issuer,
            signedPublicDailyKey,
          })
        ).toThrow(JsonWebTokenError);
      });
    });
    describe('when key is missing', () => {
      it('throws an ZodError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedPublicDailyKey = sign(
          {
            type: 'publicDailyKey',
            iss: issuer.issuerId,
            keyId,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES256' }
        );
        expect(() =>
          verifySignedPublicDailyKey({
            certificateChain,
            issuer,
            signedPublicDailyKey,
          })
        ).toThrow(z.ZodError);
      });
    });
    describe('when keyId is missing', () => {
      it('throws an ZodError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedPublicDailyKey = sign(
          {
            type: 'publicDailyKey',
            iss: issuer.issuerId,
            key,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES256' }
        );
        expect(() =>
          verifySignedPublicDailyKey({
            certificateChain,
            issuer,
            signedPublicDailyKey,
          })
        ).toThrow(z.ZodError);
      });
    });
    describe('when keyId is malformed', () => {
      it('throws an ZodError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedPublicDailyKey = sign(
          {
            type: 'publicDailyKey',
            iss: issuer.issuerId,
            keyId: '1',
            key,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES256' }
        );
        expect(() =>
          verifySignedPublicDailyKey({
            certificateChain,
            issuer,
            signedPublicDailyKey,
          })
        ).toThrow(z.ZodError);
      });
    });
    describe('When exp is given', () => {
      it('is properly passed to the sign method', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedPublicDailyKey = createSignedPublicDailyKey({
          issuer,
          privateHDSKP: hdskp.privateKey,
          keyId,
          key,
          exp: now + 1000,
        });

        const content = verifySignedPublicDailyKey({
          certificateChain,
          issuer,
          signedPublicDailyKey,
        });

        expect(content).toHaveProperty('exp');
      });
      it('is invalidated after passed expiration', () => {
        const clock = jest.useFakeTimers();
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedPublicDailyKey = createSignedPublicDailyKey({
          issuer,
          privateHDSKP: hdskp.privateKey,
          keyId,
          key,
          exp: now,
        });

        clock.advanceTimersByTime(1500);

        expect(() =>
          verifySignedPublicDailyKey({
            certificateChain,
            issuer,
            signedPublicDailyKey,
          })
        ).toThrow(TokenExpiredError);
      });
    });
    describe('When expiresIn is given', () => {
      it('is properly passed to the sign method', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedPublicDailyKey = createSignedPublicDailyKey({
          issuer,
          privateHDSKP: hdskp.privateKey,
          keyId,
          key,
          expiresIn: '10s',
        });

        const content = verifySignedPublicDailyKey({
          certificateChain,
          issuer,
          signedPublicDailyKey,
        });

        expect(content).toHaveProperty('exp');
      });
      it('is invalidated after passed expiration', async () => {
        const clock = jest.useFakeTimers();
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedPublicDailyKey = createSignedPublicDailyKey({
          issuer,
          privateHDSKP: hdskp.privateKey,
          keyId,
          key,
          expiresIn: '1s',
        });

        clock.advanceTimersByTime(1500);

        expect(() =>
          verifySignedPublicDailyKey({
            certificateChain,
            issuer,
            signedPublicDailyKey,
          })
        ).toThrow(TokenExpiredError);
      });
    });
  });
});
