import { z } from '../zod';

export const signedLocationTransferSchema = z
  .object({
    type: z.literal('locationTransfer'),
    iat: z.unixTimestamp(),
    iss: z.uuid(),
    locationId: z.uuid(),
    time: z.array(z.unixTimestamp()).length(2),
  })
  .strict();
