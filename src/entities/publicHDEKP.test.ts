import { JsonWebTokenError, TokenExpiredError } from 'jsonwebtoken';

import {
  CertificateChainInvalidError,
  CommonNameMismatchError,
} from '../errors';
import { getFingerprint } from '../x509';
import {
  createSignedPublicHDEKP,
  verifySignedPublicHDEKP,
} from './publicHDEKP';

import { hexToBase64 } from '../encoding';
import { createIssuer, createTestCertificate } from './testHelper';

const now = Math.floor(Date.now() / 1000);

describe('Crypto / Entities / publicHDEKP', () => {
  beforeEach(() => {
    jest.useFakeTimers('modern');
    jest.setSystemTime(now * 1000);
  });
  afterEach(() => {
    jest.useRealTimers();
  });

  describe('createSignedPublicHDEKP', () => {
    describe('when everything is correct', () => {
      it('creates a valid jwt', () => {
        const { issuer, hdekp, certificateChain, issuerCert } = createIssuer(
          {}
        );

        const signedPublicHDEKP = createSignedPublicHDEKP({
          issuer,
          certPrivateKey: issuerCert.privateKey,
          publicHDEKP: hdekp.publicKey,
        });

        expect(signedPublicHDEKP).toEqual(issuer.signedPublicHDEKP);

        const content = verifySignedPublicHDEKP({
          certificateChain,
          issuer: { ...issuer, signedPublicHDEKP },
        });

        expect(content).toEqual({
          iat: now,
          sub: issuer.issuerId,
          iss: getFingerprint(issuerCert.cert),
          name: issuer.name,
          key: hexToBase64(hdekp.publicKey),
          type: 'publicHDEKP',
        });
      });
      it('sets the correct issue time', () => {
        const { issuer, hdekp, certificateChain, issuerCert } = createIssuer(
          {}
        );
        const iat = 1234;

        const signedPublicHDEKP = createSignedPublicHDEKP({
          issuer,
          certPrivateKey: issuerCert.privateKey,
          publicHDEKP: hdekp.publicKey,
          iat,
        });

        const content = verifySignedPublicHDEKP({
          certificateChain,
          issuer: { ...issuer, signedPublicHDEKP },
        });

        expect(content).toEqual({
          iat,
          sub: issuer.issuerId,
          iss: getFingerprint(issuerCert.cert),
          name: issuer.name,
          key: hexToBase64(hdekp.publicKey),
          type: 'publicHDEKP',
        });
      });
    });
  });

  describe('verifySignedpublicHDEKP', () => {
    describe('when everything is correct', () => {
      it('extracts the jwts content', () => {
        const { issuer, hdekp, certificateChain, issuerCert } = createIssuer(
          {}
        );
        const content = verifySignedPublicHDEKP({
          certificateChain,
          issuer,
        });

        expect(content).toEqual({
          iat: now,
          sub: issuer.issuerId,
          iss: getFingerprint(issuerCert.cert),
          name: issuer.name,
          key: hexToBase64(hdekp.publicKey),
          type: 'publicHDEKP',
        });
      });
    });
    describe('when certificate expired', () => {
      it('throws an CertificateChainInvalidError', () => {
        const { issuer, certificateChain } = createIssuer({});
        jest.setSystemTime(now * 2000);
        expect(() =>
          verifySignedPublicHDEKP({
            certificateChain,
            issuer,
          })
        ).toThrow(CertificateChainInvalidError);
      });
    });
    describe('when certificate chain is invalid', () => {
      it('throws an CertificateChainInvalidError', () => {
        const { issuer, certificateChain } = createIssuer({});
        const { issuerCert: wrongIssuerCert } = createIssuer({});

        expect(() =>
          verifySignedPublicHDEKP({
            certificateChain: [certificateChain[0], wrongIssuerCert.cert],
            issuer,
          })
        ).toThrow(CertificateChainInvalidError);
      });
    });

    describe('when the issuer uuid is incorrect', () => {
      it('throws an JsonWebTokenError', () => {
        const { issuer, certificateChain } = createIssuer({});
        const invalidUuid = '304fdc8f-526f-4df7-abcf-000000000000';
        expect(() =>
          verifySignedPublicHDEKP({
            certificateChain,
            issuer: { ...issuer, issuerId: invalidUuid },
          })
        ).toThrow(JsonWebTokenError);
      });
    });

    describe('when isIssuing and the jwt is too old ', () => {
      it('throws an TokenExpiredError', () => {
        const { issuer, certificateChain } = createIssuer({});
        jest.setSystemTime((now + 60 * 11) * 1000);
        expect(() =>
          verifySignedPublicHDEKP({
            certificateChain,
            issuer,
            isIssuing: true,
          })
        ).toThrow(TokenExpiredError);
      });
    });
  });
});
