import * as jwt from 'jsonwebtoken';
import { z } from 'zod';
import { Hex, Base64, PEM, Issuer } from '../types';
import { base64ToHex } from '../encoding';
import { ecPublicKeyToPEM, ecPrivateKeyToPEM } from '../x509';
import { verifyJWT } from '../jwt';

import { signedPublicBadgeKeySchema } from './publicBadgeKey.schema';
import { verifySignedPublicHDSKP } from './publicHDSKP';

/**
 * Create signed public badge key token
 * @param {Issuer} issuer Issuer creating the token
 * @param {PEM} privateHDSKP private key to sign token with
 * @param {number} keyId id of the badge key
 * @param {Base64} key public badge key
 * @returns {string} token
 */
export const createSignedPublicBadgeKey = ({
  issuer,
  privateHDSKP,
  keyId,
  key,
  iat,
  exp,
  expiresIn,
}: {
  issuer: Issuer;
  privateHDSKP: Hex;
  keyId: number;
  key: Base64;
  iat?: number;
  exp?: number;
  expiresIn?: string | number;
}) => {
  return jwt.sign(
    {
      type: 'publicBadgeKey',
      iss: issuer.issuerId,
      keyId,
      key,
      ...(exp && { exp }),
      ...(iat && { iat }),
    },
    ecPrivateKeyToPEM(privateHDSKP),
    { algorithm: 'ES256', ...(expiresIn && { expiresIn }) }
  );
};

/**
 * Verify signed public badge key token
 * @param {PEM[]} certificateChain certificate chain to validate token against
 * @param {Issuer} issuer issuer who created the token
 * @param {string} token token to verify
 * @param {boolean} isIssuing indicate if the issue time should be verified
 * @returns {SignedPublicBadgeKey} validated token content
 */
export function verifySignedPublicBadgeKey({
  certificateChain,
  issuer,
  signedPublicBadgeKey,
  isIssuing = false,
}: {
  certificateChain: PEM[];
  issuer: Issuer;
  signedPublicBadgeKey: string;
  isIssuing?: boolean;
}): z.infer<typeof signedPublicBadgeKeySchema> {
  const hdskp = verifySignedPublicHDSKP({
    certificateChain,
    issuer,
  });

  return verifyJWT<z.infer<typeof signedPublicBadgeKeySchema>>({
    schema: signedPublicBadgeKeySchema,
    algorithm: 'ES256',
    token: signedPublicBadgeKey,
    publicKeyPem: ecPublicKeyToPEM(base64ToHex(hdskp.key)),
    issuer: issuer.issuerId,
    isIssuing,
  });
}
