import { sign, JsonWebTokenError } from 'jsonwebtoken';
import { v4 as uuid } from 'uuid';
import { z } from 'zod';
import { ecPrivateKeyToPEM } from '../x509';
import {
  createSignedLocationTransfer,
  verifySignedLocationTransfer,
} from './locationTransfer';
import { createIssuer } from './testHelper';

const now = Math.floor(Date.now() / 1000);
const locationId = uuid();
const time: [number, number] = [1, 1];

describe('Crypto / Entities / locationTransfer', () => {
  beforeEach(() => {
    jest.useFakeTimers('modern');
    jest.setSystemTime(now * 1000);
  });
  afterEach(() => {
    jest.useRealTimers();
  });
  describe('createSignedLocationTransfer', () => {
    describe('when everything is correct', () => {
      it('creates a valid jwt', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedLocationTransfer = createSignedLocationTransfer({
          issuer,
          privateHDSKP: hdskp.privateKey,
          locationId,
          time,
        });
        const content = verifySignedLocationTransfer({
          certificateChain,
          issuer,
          signedLocationTransfer,
        });
        expect(content).toEqual({
          iat: now,
          type: 'locationTransfer',
          iss: issuer.issuerId,
          locationId,
          time,
        });
      });
    });
    it('sets the correct issue time', () => {
      const { issuer, hdskp, certificateChain } = createIssuer({});
      const iat = 1234;
      const signedLocationTransfer = createSignedLocationTransfer({
        issuer,
        privateHDSKP: hdskp.privateKey,
        locationId,
        time,
        iat,
      });
      const content = verifySignedLocationTransfer({
        certificateChain,
        issuer,
        signedLocationTransfer,
      });
      expect(content).toEqual({
        iat,
        type: 'locationTransfer',
        iss: issuer.issuerId,
        locationId,
        time,
      });
    });
  });
  describe('verifySignedLocationTransfer', () => {
    describe('when the algorithm is incorrect', () => {
      it('throws an JsonWebTokenError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedLocationTransfer = sign(
          {
            type: 'locationTransfer',
            iss: issuer.issuerId,
            locationId,
            time,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES512' }
        );
        expect(() =>
          verifySignedLocationTransfer({
            certificateChain,
            issuer,
            signedLocationTransfer,
          })
        ).toThrow(JsonWebTokenError);
      });
    });
    describe('when time is missing', () => {
      it('throws an ZodError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedLocationTransfer = sign(
          {
            type: 'locationTransfer',
            iss: issuer.issuerId,
            locationId,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES256' }
        );
        expect(() =>
          verifySignedLocationTransfer({
            certificateChain,
            issuer,
            signedLocationTransfer,
          })
        ).toThrow(z.ZodError);
      });
    });
    describe('when locationId is missing', () => {
      it('throws an ZodError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedLocationTransfer = sign(
          {
            type: 'locationTransfer',
            iss: issuer.issuerId,
            time,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES256' }
        );
        expect(() =>
          verifySignedLocationTransfer({
            certificateChain,
            issuer,
            signedLocationTransfer,
          })
        ).toThrow(z.ZodError);
      });
    });
    describe('when time is malformed', () => {
      it('throws an ZodError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedLocationTransfer = sign(
          {
            type: 'locationTransfer',
            iss: issuer.issuerId,
            locationId,
            time: [1, '1'],
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES256' }
        );
        expect(() =>
          verifySignedLocationTransfer({
            certificateChain,
            issuer,
            signedLocationTransfer,
          })
        ).toThrow(z.ZodError);
      });
    });
  });
});
