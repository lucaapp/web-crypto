import * as jwt from 'jsonwebtoken';
import { z } from 'zod';
import { PEM, Issuer, BaseIssuer, Hex } from '../types';
import { verifyCertificateChain, getFingerprint, getPublicKey } from '../x509';
import { CertificateChainInvalidError } from '../errors';
import { verifyJWT } from '../jwt';
import { hexToBase64 } from '../encoding';

import { signedPublicHDSKPSchema } from './publicHDSKP.schema';

/**
 * Create signed public HDSKP token
 * @param {BaseIssuer} issuer Issuer creating the token
 * @param {Hex} privateKey private key to sign token with
 * @param {Base64} publicHDSKP public HDSKP
 * @returns {string} token
 */
export const createSignedPublicHDSKP = ({
  issuer,
  certPrivateKey,
  publicHDSKP,
  iat,
}: {
  issuer: BaseIssuer;
  certPrivateKey: PEM;
  publicHDSKP: Hex;
  iat?: number;
}) =>
  jwt.sign(
    {
      sub: issuer.issuerId,
      iss: getFingerprint(issuer.publicCertificate),
      name: issuer.name,
      key: hexToBase64(publicHDSKP),
      type: 'publicHDSKP',
      ...(iat && { iat }),
    },
    certPrivateKey,
    { algorithm: 'RS512' }
  );

/**
 * Verify signed public HDSKP token
 * @param {PEM[]} certificateChain certificate chain to validate token against
 * @param {Issuer} issuer issuer who created the token
 * @param {boolean} isIssuing indicate if the issue time should be verified
 * @returns {SignedPublicHDSKP} validated token content
 */
export const verifySignedPublicHDSKP = ({
  certificateChain,
  issuer,
  isIssuing = false,
}: {
  certificateChain: PEM[];
  issuer: Issuer;
  isIssuing?: boolean;
}): z.infer<typeof signedPublicHDSKPSchema> => {
  const isChainValid = verifyCertificateChain([
    ...certificateChain,
    issuer.publicCertificate,
  ]);

  if (!isChainValid) {
    throw new CertificateChainInvalidError('Certificate chain is invalid.');
  }

  return verifyJWT<z.infer<typeof signedPublicHDSKPSchema>>({
    schema: signedPublicHDSKPSchema,
    algorithm: 'RS512',
    token: issuer.signedPublicHDSKP,
    publicKeyPem: getPublicKey(issuer.publicCertificate),
    issuer: getFingerprint(issuer.publicCertificate),
    subject: issuer.issuerId,
    isIssuing,
  });
};
