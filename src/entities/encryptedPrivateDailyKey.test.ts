import { sign, JsonWebTokenError, TokenExpiredError } from 'jsonwebtoken';
import { v4 as uuid } from 'uuid';
import { z } from 'zod';
import { ecPrivateKeyToPEM } from '../x509';
import {
  createSignedEncryptedPrivateDailyKey,
  verifySignedEncryptedPrivateDailyKey,
} from './encryptedPrivateDailyKey';
import { createIssuer } from './testHelper';

const now = Math.floor(Date.now() / 1000);
const subject = uuid();
const keyId = 0;
const key = {
  data: 'j5MDJV5RJjn4/vvTevzBC13h87ZlbVSG7DUrC2Gnx84=',
  iv: 'N9LNm4bqAC7s7IrTj0OAdA==',
  mac: 'v7jp+EF9udYV8d42aKtm7V8/BSwQasCn5jIF1NB0+B8=',
  publicKey:
    'BLqXxmUfw4VLA2U/SsOkxN/e2/B8zH+pBFl33knnjv/SVSZY9hllfvayB9ZqCKa2WAZiviidPnTtW385tlxSv/I=',
};

describe('Crypto / Entities / encryptedPrivateDailyKey', () => {
  beforeEach(() => {
    jest.useFakeTimers('modern');
    jest.setSystemTime(now * 1000);
  });
  afterEach(() => {
    jest.useRealTimers();
  });
  describe('createSignedEncryptedPrivateDailyKey', () => {
    describe('when everything is correct', () => {
      it('creates a valid jwt', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedEncryptedPrivateDailyKey =
          createSignedEncryptedPrivateDailyKey({
            issuer,
            privateHDSKP: hdskp.privateKey,
            subject,
            keyId,
            key,
          });
        const content = verifySignedEncryptedPrivateDailyKey({
          certificateChain,
          issuer,
          signedEncryptedPrivateDailyKey,
        });
        expect(content).toEqual({
          iat: now,
          type: 'encryptedPrivateDailyKey',
          iss: issuer.issuerId,
          sub: subject,
          keyId,
          key,
        });
      });
      it('sets the correct issue time', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const iat = 1234;
        const signedEncryptedPrivateDailyKey =
          createSignedEncryptedPrivateDailyKey({
            issuer,
            privateHDSKP: hdskp.privateKey,
            subject,
            keyId,
            key,
            iat,
          });
        const content = verifySignedEncryptedPrivateDailyKey({
          certificateChain,
          issuer,
          signedEncryptedPrivateDailyKey,
        });
        expect(content).toEqual({
          iat,
          type: 'encryptedPrivateDailyKey',
          iss: issuer.issuerId,
          sub: subject,
          keyId,
          key,
        });
      });
    });
  });
  describe('verifySignedEncryptedPrivateDailyKey', () => {
    describe('when the algorithm is incorrect', () => {
      it('throws an JsonWebTokenError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedEncryptedPrivateDailyKey = sign(
          {
            type: 'encryptedPrivateDailyKey',
            iss: issuer.issuerId,
            keyId,
            key,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES512' }
        );
        expect(() =>
          verifySignedEncryptedPrivateDailyKey({
            certificateChain,
            issuer,
            signedEncryptedPrivateDailyKey,
          })
        ).toThrow(JsonWebTokenError);
      });
    });
    describe('when key is missing', () => {
      it('throws an ZodError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedEncryptedPrivateDailyKey = sign(
          {
            type: 'encryptedPrivateDailyKey',
            iss: issuer.issuerId,
            keyId,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES256' }
        );
        expect(() =>
          verifySignedEncryptedPrivateDailyKey({
            certificateChain,
            issuer,
            signedEncryptedPrivateDailyKey,
          })
        ).toThrow(z.ZodError);
      });
    });
    describe('when keyId is missing', () => {
      it('throws an ZodError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedEncryptedPrivateDailyKey = sign(
          {
            type: 'encryptedPrivateDailyKey',
            iss: issuer.issuerId,
            key,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES256' }
        );
        expect(() =>
          verifySignedEncryptedPrivateDailyKey({
            certificateChain,
            issuer,
            signedEncryptedPrivateDailyKey,
          })
        ).toThrow(z.ZodError);
      });
    });
    describe('when keyId is malformed', () => {
      it('throws an ZodError', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedEncryptedPrivateDailyKey = sign(
          {
            type: 'encryptedPrivateDailyKey',
            iss: issuer.issuerId,
            keyId: '1',
            key,
          },
          ecPrivateKeyToPEM(hdskp.privateKey),
          { algorithm: 'ES256' }
        );
        expect(() =>
          verifySignedEncryptedPrivateDailyKey({
            certificateChain,
            issuer,
            signedEncryptedPrivateDailyKey,
          })
        ).toThrow(z.ZodError);
      });
    });
    describe('When exp is given', () => {
      it('is properly passed to the sign method', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedEncryptedPrivateDailyKey =
          createSignedEncryptedPrivateDailyKey({
            issuer,
            privateHDSKP: hdskp.privateKey,
            subject,
            keyId,
            key,
            exp: now + 1000,
          });

        const content = verifySignedEncryptedPrivateDailyKey({
          certificateChain,
          issuer,
          signedEncryptedPrivateDailyKey,
        });

        expect(content).toHaveProperty('exp');
      });
      it('is invalidated after passed expiration', () => {
        const clock = jest.useFakeTimers();

        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedEncryptedPrivateDailyKey =
          createSignedEncryptedPrivateDailyKey({
            issuer,
            privateHDSKP: hdskp.privateKey,
            subject,
            keyId,
            key,
            exp: now,
          });

        clock.advanceTimersByTime(1500);

        expect(() =>
          verifySignedEncryptedPrivateDailyKey({
            certificateChain,
            issuer,
            signedEncryptedPrivateDailyKey,
          })
        ).toThrow(TokenExpiredError);
      });
    });
    describe('When expiresIn is given', () => {
      it('is properly passed to the sign method', () => {
        const { issuer, hdskp, certificateChain } = createIssuer({});

        const signedEncryptedPrivateDailyKey =
          createSignedEncryptedPrivateDailyKey({
            issuer,
            privateHDSKP: hdskp.privateKey,
            subject,
            keyId,
            key,
            expiresIn: '10s',
          });

        const content = verifySignedEncryptedPrivateDailyKey({
          certificateChain,
          issuer,
          signedEncryptedPrivateDailyKey,
        });

        expect(content).toHaveProperty('exp');
      });
      it('is invalidated after passed expiration', () => {
        const clock = jest.useFakeTimers();

        const { issuer, hdskp, certificateChain } = createIssuer({});
        const signedEncryptedPrivateDailyKey =
          createSignedEncryptedPrivateDailyKey({
            issuer,
            privateHDSKP: hdskp.privateKey,
            subject,
            keyId,
            key,
            expiresIn: '1s',
          });

        clock.advanceTimersByTime(1500);

        expect(() =>
          verifySignedEncryptedPrivateDailyKey({
            certificateChain,
            issuer,
            signedEncryptedPrivateDailyKey,
          })
        ).toThrow(TokenExpiredError);
      });
    });
  });
});
