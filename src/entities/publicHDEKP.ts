import * as jwt from 'jsonwebtoken';
import { z } from 'zod';
import { PEM, Issuer, BaseIssuer, Hex } from '../types';
import { verifyCertificateChain, getFingerprint, getPublicKey } from '../x509';
import { CertificateChainInvalidError } from '../errors';
import { verifyJWT } from '../jwt';
import { hexToBase64 } from '../encoding';

import { signedPublicHDEKPSchema } from './publicHDEKP.schema';

/**
 * Create signed public HDEKP token
 * @param {BaseIssuer} issuer Issuer creating the token
 * @param {Hex} privateKey private key to sign token with
 * @param {Base64} publicHDEKP public HDEKP
 * @returns {string} token
 */
export const createSignedPublicHDEKP = ({
  issuer,
  certPrivateKey,
  publicHDEKP,
  iat,
}: {
  issuer: BaseIssuer;
  certPrivateKey: PEM;
  publicHDEKP: Hex;
  iat?: number;
}) =>
  jwt.sign(
    {
      sub: issuer.issuerId,
      iss: getFingerprint(issuer.publicCertificate),
      name: issuer.name,
      key: hexToBase64(publicHDEKP),
      type: 'publicHDEKP',
      ...(iat && { iat }),
    },
    certPrivateKey,
    { algorithm: 'RS512' }
  );

/**
 * Verify signed public HDEKP token
 * @param {PEM[]} certificateChain certificate chain to validate token against
 * @param {Issuer} issuer issuer who created the token
 * @param {boolean} isIssuing indicate if the issue time should be verified
 * @returns {SignedPublicHDEKP} validated token content
 */
export const verifySignedPublicHDEKP = ({
  certificateChain,
  issuer,
  isIssuing = false,
}: {
  certificateChain: PEM[];
  issuer: Required<Issuer>;
  isIssuing?: boolean;
}): z.infer<typeof signedPublicHDEKPSchema> => {
  const isChainValid = verifyCertificateChain([
    ...certificateChain,
    issuer.publicCertificate,
  ]);

  if (!isChainValid) {
    throw new CertificateChainInvalidError('Certificate chain is invalid.');
  }

  return verifyJWT<z.infer<typeof signedPublicHDEKPSchema>>({
    schema: signedPublicHDEKPSchema,
    algorithm: 'RS512',
    token: issuer.signedPublicHDEKP,
    publicKeyPem: getPublicKey(issuer.publicCertificate),
    issuer: getFingerprint(issuer.publicCertificate),
    subject: issuer.issuerId,
    isIssuing,
  });
};
