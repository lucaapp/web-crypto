export class CommonNameMismatchError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'CommonNameMismatchError';
  }
}

export class CertificateChainInvalidError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'CertificateChainInvalidError';
  }
}
