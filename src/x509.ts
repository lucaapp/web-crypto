import * as forge from 'node-forge';
import { PEM, Hex } from './types';
import { hexToBase64 } from './encoding';
import { EC_KEYPAIR_FROM_PRIVATE_KEY } from './crypto';

const PEM_PUBLIC_PREFIX =
  '3059301306072A8648CE3D020106082A8648CE3D030107034200';
const PEM_PRIVATE_PRIVATE_PREFIX = '30770201010420';
const PEM_PRIVATE_PUBLIC_PREFIX = 'A00A06082A8648CE3D030107A144034200';

/**
 * Generate PEM from hex EC public key
 * @param {Hex} publicKey public key which will be stored in the pem
 */
export const ecPublicKeyToPEM = (publicKey: Hex): string => {
  return `-----BEGIN PUBLIC KEY-----
${hexToBase64(PEM_PUBLIC_PREFIX + publicKey)}
-----END PUBLIC KEY-----`;
};

/**
 * Generate PEM from EC private key
 * @param {Hex} privateKey private key which will be stored in the pem
 */
export const ecPrivateKeyToPEM = (privateKey: Hex): string => {
  const { publicKey } = EC_KEYPAIR_FROM_PRIVATE_KEY(privateKey);
  return `-----BEGIN EC PRIVATE KEY-----
${hexToBase64(
  PEM_PRIVATE_PRIVATE_PREFIX +
    privateKey +
    PEM_PRIVATE_PUBLIC_PREFIX +
    publicKey
)}
-----END EC PRIVATE KEY-----`;
};

/**
 * Extract common name from PEM certificate
 * @param {PEM} pem PEM certificate to extract from
 * @returns {string} common name
 */
export const getCommonName = (pem: PEM): Hex => {
  const certificate = forge.pki.certificateFromPem(pem);
  return certificate.subject.getField('CN')?.value;
};

/**
 * Extract organizational unit name from PEM certificate
 * @param {PEM} pem PEM certificate to extract from
 * @returns {string} organizational unit name
 */
export const getOrganizationalUnitName = (pem: PEM): Hex => {
  const certificate = forge.pki.certificateFromPem(pem);
  return certificate.subject.getField('OU')?.value;
};

/**
 * Calculate fingerprint of PEM certificate
 * @param {PEM} pem PEM to calculate fingerprint from
 * @returns {string} common name
 */
export const getFingerprint = (pem: PEM): Hex => {
  const certificate = forge.pki.certificateFromPem(pem);
  const certDer = forge.asn1.toDer(forge.pki.certificateToAsn1(certificate));
  const md = forge.md.sha1.create();
  md.update(certDer.data);
  return md.digest().toHex();
};

/**
 * Extract public key from PEM certificate
 * @param {PEM} pem PEM certificate to extract public key from
 * @returns {PEM} public key as PEM
 */
export const getPublicKey = (pem: PEM): PEM => {
  const certificate = forge.pki.certificateFromPem(pem);
  return forge.pki.publicKeyToPem(certificate.publicKey);
};

/**
 * Verify PEM certificate chain
 * @param {PEM[]} certificateChain PEM certificate chain to validate
 * @returns {boolean} isChainValid
 */
export const verifyCertificateChain = (certificateChain: PEM[]): boolean => {
  const certificates = certificateChain.map(pem =>
    forge.pki.certificateFromPem(pem)
  );
  const caStore = forge.pki.createCaStore([certificates[0]]);

  const chain = certificates.slice(1).reverse();

  let isChainValid;
  try {
    isChainValid = forge.pki.verifyCertificateChain(caStore, chain);
  } catch (error) {
    isChainValid = false;
  }
  return isChainValid;
};
