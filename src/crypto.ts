import { hkdf as sha256HKDF } from 'fast-sha256';

import { scrypt } from 'scrypt-js';
import * as forge from 'node-forge';

import { EllipticCurve } from './EllipticCurve';
import { Hex, Base64, ECKeyPair } from './types';
import { hexToBytes, bytesToUint8Array, uint8ArrayToBytes } from './encoding';

/**
 * A detailed description for the chosen cryptographic algorithms can
 * be found here:
 * https://luca-app.de/securityoverview/appendix/algorithms.html
 */

/**
 * Generate random values
 *
 * @param {number} n Number of random bytes requested
 * @returns {Hex} random value
 */
export const GET_RANDOM_BYTES = (n: number): Hex =>
  forge.util.bytesToHex(forge.random.getBytesSync(n));

/**
 * Generate EC key pair
 *
 * @returns {ECKeyPair} EC key pair
 */
export const EC_KEYPAIR_GENERATE = (): ECKeyPair => {
  const keyPair = EllipticCurve.getInstance().genKeyPair();

  return {
    privateKey: keyPair.getPrivate('hex'),
    publicKey: keyPair.getPublic('hex'),
    compressedPublicKey: keyPair.getPublic(true, 'hex'),
  };
};

/**
 * derive EC public key from private key
 *
 * @param {Hex} privateKey privateKey for which the public key should be calculated
 * @returns {ECKeyPair} KeyPair and compressed public key
 */
export const EC_KEYPAIR_FROM_PRIVATE_KEY = (privateKey: Hex): ECKeyPair => {
  const keyPair = EllipticCurve.getInstance().keyFromPrivate(privateKey, 'hex');
  return {
    privateKey: keyPair.getPrivate('hex'),
    publicKey: keyPair.getPublic('hex'),
    compressedPublicKey: keyPair.getPublic(true, 'hex'),
  };
};

/**
 * Sign data with ECDSA in DER encoding using SHA256 as hash
 *
 * @param {Hex} privateKey  PrivateKey used to sign data
 * @param {Hex} data Data to be signed
 * @returns {Hex} DER encoded signature
 */
export const SIGN_EC_SHA256_DER = (privateKey: Hex, data: Hex): Hex => {
  const ecPrivateKey = EllipticCurve.getInstance().keyFromPrivate(privateKey, 'hex');
  const md = forge.md.sha256.create();
  md.update(hexToBytes(data));
  const signature = ecPrivateKey.sign(md.digest().toHex());
  return signature.toDER('hex');
};

/**
 * Verifies ECDSA signature in DER encoding using SHA256 as hash
 *
 * @param {Hex} publicKey PublicKey to verify signature
 * @param {Hex} data signed data to be verified
 * @param {Hex} signature signature of data
 * @returns {boolean} true if signature is valid
 */
export const VERIFY_EC_SHA256_DER_SIGNATURE = (
  publicKey: Hex,
  data: Hex,
  signature: Hex
): boolean => {
  try {
    const ecPublicKey = EllipticCurve.getInstance().keyFromPublic(publicKey, 'hex');
    const md = forge.md.sha256.create();
    md.update(hexToBytes(data));
    return ecPublicKey.verify(md.digest().toHex(), signature);
  } catch {
    return false;
  }
};

/**
 * Sign data with ECDSA in IEEE encoding using SHA256 as hash
 *
 * @param {Hex} privateKey PrivateKey used to sign data
 * @param {Hex} data Data to be signed
 * @returns {Hex} IEEE encoded signature
 */
export const SIGN_EC_SHA256_IEEE = (privateKey: Hex, data: Hex): Hex => {
  const ecPrivateKey = EllipticCurve.getInstance().keyFromPrivate(privateKey, 'hex');
  const md = forge.md.sha256.create();
  md.update(hexToBytes(data));
  const signature = ecPrivateKey.sign(md.digest().toHex());
  const r = signature.r.toString(16, 32);
  const s = signature.s.toString(16, 32);
  return r + s;
};

/**
 * Verifys ECDSA signature in IEEE encoding using SHA256 as hash
 *
 * @param {Hex} publicKey PublicKey to verify signature
 * @param {Hex} data signed data to be verified
 * @param {Hex} signature signature of data
 * @returns {boolean} true if signature is valid
 */
export const VERIFY_EC_SHA256_IEEE_SIGNATURE = (
  publicKey: Hex,
  data: Hex,
  signature: Hex
): boolean => {
  try {
    const ecPublicKey = EllipticCurve.getInstance().keyFromPublic(publicKey, 'hex');
    const md = forge.md.sha256.create();
    md.update(hexToBytes(data));
    const sig = {
      r: signature.slice(0, 64),
      s: signature.slice(64, 128),
    };
    return ecPublicKey.verify(md.digest().toHex(), sig);
  } catch {
    return false;
  }
};

/**
 * Calculate ECDH_VARIABLE_SIZE shared secret with variable size
 *
 * @param {Hex} privateKey Private key
 * @param {Hex} publicKey Public key
 * @returns {Hex} Shared secret
 */
export const ECDH_VARIABLE_SIZE = (privateKey: Hex, publicKey: Hex): Hex => {
  const ecPrivateKey = EllipticCurve.getInstance().keyFromPrivate(privateKey, 'hex');
  const ecPublicKey = EllipticCurve.getInstance().keyFromPublic(publicKey, 'hex');
  const sharedSecret = ecPrivateKey.derive(ecPublicKey.getPublic());

  return sharedSecret.toString('hex');
};

/**
 * Calculate ECDH shared secret
 *
 * @param {Hex} privateKey Private key
 * @param {Hex} publicKey Public key
 * @returns {Hex} Shared secret
 */
export const ECDH = (privateKey: Hex, publicKey: Hex): Hex => {
  const ecPrivateKey = EllipticCurve.getInstance().keyFromPrivate(privateKey, 'hex');
  const ecPublicKey = EllipticCurve.getInstance().keyFromPublic(publicKey, 'hex');
  const sharedSecret = ecPrivateKey.derive(ecPublicKey.getPublic());

  return sharedSecret.toString('hex').padStart(64, '0');
};

/**
 * Calculates SHA256 hash
 *
 * @param {Hex} data Data to be hashed
 * @returns {Hex} Hash of data
 */
export const SHA256 = (data: Hex): Hex => {
  const md = forge.md.sha256.create();
  md.update(hexToBytes(data));
  return md.digest().toHex();
};

/**
 * Calculate HMAC of data using SHA256 as hash
 *
 * @param {Hex} data Data for which the HMAC should be calculated
 * @param {Hex} key Key used for calculating HMAC
 * @returns {Hex} HMAC of data
 */
export const HMAC_SHA256 = (data: Hex, key: Hex): Hex => {
  const hmac = forge.hmac.create();
  hmac.start('sha256', hexToBytes(key));
  hmac.update(hexToBytes(data));
  return hmac.digest().toHex();
};

/**
 * HMAC based key derivation function using SHA256 as hash
 *
 * @param {Hex} data Data used to derive key
 * @param {number} length
 * @param {Hex} info Value to to bind the derived key material to application-
 *                   and context-specific information
 * @param {Hex} salt Salt used to derive key
 * @returns {Hex} Derived key
 */
export const HKDF_SHA256 = async (
  data: Hex,
  length: number,
  info: Hex,
  salt: Hex
): Promise<Hex> => {
  return forge.util
    .createBuffer(
      sha256HKDF(
        bytesToUint8Array(hexToBytes(data)),
        bytesToUint8Array(hexToBytes(salt)),
        bytesToUint8Array(hexToBytes(info)),
        length
      )
    )
    .toHex();
};

/**
 * SHA256 based key derivation function
 *
 * @param {Hex} seed Seed used to derive key
 * @param {Hex} keyId keyID used to derive key
 * @returns {Hex} Derived key
 */
export const KDF_SHA256 = (seed: Hex, keyId: Hex): Hex => SHA256(seed + keyId);

/**
 * Generate key for AES encryption based on a password and a salt
 * @param {Bytes} secret
 * @param {Bytes} salt
 * @param {Number} n cost factor
 * @param {Number} r block size
 * @param {Number} p parallelization cost
 * @param {Number} dkLen generated key length
 */
export const GENERATE_KEY_FROM_SECRET = async (
  secret: Hex,
  salt: Hex,
  n: number = 65536,
  r = 8,
  p = 1,
  dkLen: number = 32
): Promise<string> => {
  const key = await scrypt(
    bytesToUint8Array(hexToBytes(secret)),
    bytesToUint8Array(hexToBytes(salt)),
    n,
    r,
    p,
    dkLen
  );
  return uint8ArrayToBytes(key);
};

/**
 * Encrypt data using AES in CTR mode
 *
 * @param {Hex} data Data to encrypt
 * @param {Hex} key Key used for encryption
 * @param {Hex} iv Initialization vector used
 * @returns {Hex} Encrypted data
 */
export const ENCRYPT_AES_CTR = (data: Hex, key: Hex, iv: Hex): Hex => {
  const cipher = forge.cipher.createCipher('AES-CTR', hexToBytes(key));
  cipher.start({ iv: hexToBytes(iv) });
  cipher.update(forge.util.createBuffer(hexToBytes(data)));
  cipher.finish();

  return cipher.output.toHex();
};

/**
 * Decrypt data using AES in CTR mode
 *
 * @param {Hex} data Data to decrypt
 * @param {Hex} key Key used for decryption
 * @param {Hex} iv Initialization vector
 * @returns {Hex} Decrypted data
 */
export const DECRYPT_AES_CTR = (data: Hex, key: Hex, iv: Hex): Hex => {
  const cipher = forge.cipher.createDecipher('AES-CTR', hexToBytes(key));
  cipher.start({ iv: hexToBytes(iv) });
  cipher.update(forge.util.createBuffer(hexToBytes(data)));
  cipher.finish();

  return cipher.output.toHex();
};

/**
 * Encrypt data using AES in GCM mode
 *
 * @param {Hex} data Data to encrypt
 * @param {Hex} key Key used for encryption
 * @param {Hex} iv Initialization vector
 * @returns {Hex} Encrypted data
 * @returns {Hex} Authentication tag
 */
export const ENCRYPT_AES_GCM = (data: Hex, key: Hex, iv: Hex) => {
  const cipher = forge.cipher.createCipher('AES-GCM', hexToBytes(key));
  cipher.start({ iv: hexToBytes(iv) });
  cipher.update(forge.util.createBuffer(hexToBytes(data)));
  cipher.finish();

  const encrypted = cipher.output.toHex();
  const tag = cipher.mode.tag.toHex();

  return { encrypted, tag };
};

/**
 * Decrypt data using AES in GCM mode
 *
 * @param {Hex} data Data to decrypt
 * @param {Hex} tag Authentication tag used to verify the integrity of the data
 * @param {Hex} key Key used for decryption
 * @param {Hex} iv Initialization vector
 * @returns {Hex} Decrypted data
 */
export const DECRYPT_AES_GCM = (
  data: Hex,
  tag: Hex,
  key: Hex,
  iv: Hex
): Hex => {
  const cipher = forge.cipher.createDecipher('AES-GCM', hexToBytes(key));
  cipher.start({
    iv: hexToBytes(iv),
    tag: forge.util.createBuffer(hexToBytes(tag)),
  });
  cipher.update(forge.util.createBuffer(hexToBytes(data)));
  const pass = cipher.finish();

  if (!pass) {
    throw new Error('Invalid authentication tag');
  }

  return cipher.output.toHex();
};

/**
 * Encrypt data using DLIES
 *
 * @param {Hex} publicKey public key of the receiver
 * @param {Hex} data Data to be encrypted
 * @returns {Hex} Public key for ECDH (part of DLIES)
 * @returns {Hex} Encrypted data
 * @returns {Hex} Initialization vector
 * @returns {Hex} HMAC (SHA256)
 */
export const ENCRYPT_DLIES = (publicKey: Hex, data: Hex) => {
  const ephemeralKeyPair = EC_KEYPAIR_GENERATE();
  const dhKey = ECDH(ephemeralKeyPair.privateKey, publicKey);
  const encryptionKey = KDF_SHA256(dhKey, '01').slice(0, 32);
  const authKey = KDF_SHA256(dhKey, '02');
  const iv = GET_RANDOM_BYTES(16);
  const encryptedData = ENCRYPT_AES_CTR(data, encryptionKey, iv);
  const mac = HMAC_SHA256(encryptedData, authKey);

  return {
    publicKey: ephemeralKeyPair.publicKey,
    data: encryptedData,
    iv,
    mac,
  };
};

/**
 * Decrypt data using DLIES
 *
 * @param {Hex} privateKey Private key of the receiver
 * @param {Hex} publicKey Public key of the sender
 * @param {Hex} data Data to decrypt
 * @param {Hex} iv Initialization vector
 * @param {Hex} mac HMAC (SHA256)
 * @returns {Hex} Decrypted data
 */
export const DECRYPT_DLIES = (
  privateKey: Hex,
  publicKey: Hex,
  data: Hex,
  iv: Hex,
  mac: Hex
): Hex => {
  let dhKey = ECDH(privateKey, publicKey);
  let encKey = KDF_SHA256(dhKey, '01').slice(0, 32);
  let authKey = KDF_SHA256(dhKey, '02');
  let macCheck = HMAC_SHA256(data, authKey);

  // fallback to variable sized ECDH
  if (mac !== macCheck) {
    dhKey = ECDH_VARIABLE_SIZE(privateKey, publicKey);
    encKey = KDF_SHA256(dhKey, '01').slice(0, 32);
    authKey = KDF_SHA256(dhKey, '02');
    macCheck = HMAC_SHA256(data, authKey);

    if (mac !== macCheck) {
      throw new Error('Invalid MAC');
    }
  }

  return DECRYPT_AES_CTR(data, encKey, iv);
};

/**
 * Decrypt data using DLIES - does not check HMAC
 *
 * @param {Hex} privateKey Private key of the receiver
 * @param {Hex} publicKey Public key of the sender
 * @param {Hex} data Data to decrypt
 * @param {Hex} iv Initialization vector
 * @returns {Hex} Decrypted data
 */
export const DECRYPT_DLIES_WITHOUT_MAC = (
  privateKey: Hex,
  publicKey: Hex,
  data: Hex,
  iv: Hex
) => {
  const dhKey = ECDH(privateKey, publicKey);
  const encKey = KDF_SHA256(dhKey, '01').slice(0, 32);

  return DECRYPT_AES_CTR(data, encKey, iv);
};

/**
 * Generate trace ID
 *
 * @param {Hex} data Data used to generate trace ID
 * @param {Hex} key Key used to generate trace ID
 * @returns {Base64} Trace ID
 */
export const GENERATE_TRACE_ID = (data: Hex, key: Hex): Base64 => {
  const hmac = forge.hmac.create();
  hmac.start('sha256', hexToBytes(key));
  hmac.update(hexToBytes(data));
  return forge.util.encode64(hmac.digest().bytes().slice(0, 16));
};
