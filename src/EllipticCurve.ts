import { ec as Elliptic } from 'elliptic';

export class EllipticCurve {
  private static instance: Elliptic;

  public static getInstance(): Elliptic {
    if (!this.instance) {
      this.instance = new Elliptic('p256');
    }

    return this.instance;
  }
}