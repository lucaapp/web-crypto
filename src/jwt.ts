import * as jwt from 'jsonwebtoken';
import { z } from 'zod';
import { PEM } from './types';

export function verifyJWT<jwtSchemaType>({
  schema,
  algorithm,
  token,
  publicKeyPem,
  issuer,
  subject,
  isIssuing,
}: {
  schema: z.ZodSchema<jwtSchemaType>;
  algorithm: jwt.Algorithm;
  token: string;
  publicKeyPem: PEM;
  issuer: string;
  subject?: string;
  isIssuing: boolean;
}): jwtSchemaType {
  const content = jwt.verify(token, publicKeyPem, {
    algorithms: [algorithm],
    issuer,
    subject,
    ...(isIssuing && { maxAge: '10 minutes' }),
  });
  return schema.parse(content);
}
