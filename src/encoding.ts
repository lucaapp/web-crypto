import * as forge from 'node-forge';

// @ts-ignore
import { ZeroMQ as z85 } from 'ascii85';

import { Hex, Bytes, Base64, Base32, Base85, Z85, Bit } from './types';
import base32Decode from 'base32-decode';
import base32Encode from 'base32-encode';

export const bytesToHex = (data: Bytes): Hex => forge.util.bytesToHex(data);
export const hexToBytes = (data: Hex): Bytes =>
  forge.util.hexToBytes(data.trim());
export const decodeUtf8 = (data: Bytes): Bytes => forge.util.decodeUtf8(data);
export const encodeUtf8 = (data: Bytes): Bytes => forge.util.encodeUtf8(data);
export const bytesToBase64 = (data: Bytes): Base64 => forge.util.encode64(data);
export const base64ToBytes = (data: Base64): Bytes =>
  forge.util.decode64(data.trim());

export const bytesToBuffer = (data: Bytes) => forge.util.createBuffer(data);

export const hexToZ85 = (data: Hex): Base85 => {
  return z85.encode(forge.util.hexToBytes(data.trim())).toString();
};

export const z85ToBytes = (data: Z85): Bytes => z85.decode(data.trim());
export const bytesToZ85 = (data: Bytes): Z85 => z85.encode(data);

export const z85ToHex = (data: Base85): Hex => {
  return forge.util.bytesToHex(z85.decode(data.trim()));
};

export const base64UrlToBytes = (data: string): Bytes => {
  return atob(data.replace(/-/g, '+').replace(/_/g, '/'));
};

export const hexToBase64 = (data: Hex): Base64 =>
  forge.util.encode64(forge.util.hexToBytes(data.trim()));

export const base64ToHex = (data: Base64): Hex =>
  forge.util.bytesToHex(forge.util.decode64(data.trim()));

export const hexToBase32 = (data: Hex): Base32 =>
  base32Encode(forge.util.binary.hex.decode(data.trim()), 'RFC4648');

export const base32ToHex = (data: Base32): Hex =>
  forge.util.createBuffer(base32Decode(data.trim(), 'RFC4648')).toHex();

export const hexToBase32Crockford = (data: Hex): Base32 =>
  base32Encode(forge.util.binary.hex.decode(data.trim()), 'Crockford');

export const base32CrockfordToHex = (data: Base32): Hex =>
  forge.util.createBuffer(base32Decode(data.trim(), 'Crockford')).toHex();

export const hexToInt8 = (data: Hex): number =>
  forge.util.hexToBytes(data.trim()).charCodeAt(0);

export const int8ToHex = (n: number): Hex =>
  forge.util.bytesToHex(String.fromCharCode(n));

export const int32ToHex = (n: number): Hex => {
  const buffer = forge.util.createBuffer();
  buffer.putInt32Le(n);
  return forge.util.bytesToHex(buffer.getBytes());
};

export const int32ToUint8Array = (n: number): Uint8Array => {
  const bytes = new Uint8Array(4);
  for (let index = 3; index >= 0; index -= 1) {
    /* tslint:disable:no-bitwise */
    bytes[index] = n & 0xff;
    n >>= 8;
    /* tslint:enable:no-bitwise */
  }
  return bytes;
};

export const uint8ArrayToInt32 = (byteArray: number[]): number => {
  let int32 = 0;
  for (const [index, byte] of byteArray.entries()) {
    int32 += byte * 2 ** (24 - index * 8);
  }
  return int32;
};

export const uint8ArrayToInt64 = (byteArray: number[]): number => {
  let int64 = 0;
  for (const [index, byte] of byteArray.entries()) {
    int64 += byte * 2 ** (56 - index * 8);
  }
  return int64;
};

export const int64ToUint8Array = (n: number): Uint8Array => {
  const part1 = int32ToUint8Array(n / 2 ** 32);
  const part2 = int32ToUint8Array(n);
  const array = new Uint8Array(part1.length + part2.length);
  array.set(part1);
  array.set(part2, part1.length);
  return array;
};

// This function will only work for NodeJS Applications
export const bitArrayToUint8Array = (array: Bit[]): Uint8Array => {
  const byteArray = [];

  for (let index = 0; index < array.length; index += 8) {
    let outputByte = 0;
    /* tslint:disable:no-bitwise */
    outputByte += array[index] << 7;
    outputByte += array[index + 1] << 6;
    outputByte += array[index + 2] << 5;
    outputByte += array[index + 3] << 4;
    outputByte += array[index + 4] << 3;
    outputByte += array[index + 5] << 2;
    outputByte += array[index + 6] << 1;
    outputByte += array[index + 7];
    /* tslint:enable:no-bitwise */
    byteArray.push(outputByte);
  }
  return Uint8Array.from(byteArray);
};

export const uint8ArrayToBitArray = (bitmap: Uint8Array): Bit[] => {
  const array = [];
  for (const byte of bitmap) {
    /* tslint:disable:no-bitwise */
    array.push(
      (byte & 0x80) >> 7,
      (byte & 0x40) >> 6,
      (byte & 0x20) >> 5,
      (byte & 0x10) >> 4,
      (byte & 0x08) >> 3,
      (byte & 0x04) >> 2,
      (byte & 0x02) >> 1,
      byte & 0x01
    );
    /* tslint:enable:no-bitwise */
  }

  // @ts-ignore
  return array;
};

// This function will only work for NodeJS Applications
export const int32ToBase64 = (n: number): Base64 => {
  const buffer = Buffer.alloc(4);
  buffer.writeUInt32LE(n);
  return buffer.toString('base64');
};

export const uuidToHex = (uuid: string): Hex => {
  return uuid.trim().replace(/-/g, '');
};

export const hexToUuid = (data: Hex): string =>
  `${data.slice(0, 8)}-${data.slice(8, 12)}-${data.slice(12, 16)}-${data.slice(
    16,
    20
  )}-${data.slice(20, 32)}`;

export const bytesToUint8Array = (data: Bytes): Uint8Array => {
  const byteArray = [];
  for (let index = 0; index < data.length; index += 1) {
    byteArray.push(data.charCodeAt(index));
  }
  return Uint8Array.from(byteArray);
};

export const uint8ArrayToBytes = (uint8Array: Uint8Array): Bytes => {
  let bytes: Bytes = '';

  for (let index = 0; index < uint8Array.byteLength; index++) {
    bytes += String.fromCharCode(uint8Array[index]);
  }

  return bytes;
};

export const hexToUuid4 = (hexUuid: Hex): string => {
  const uuidSeed = bytesToUint8Array(forge.util.hexToBytes(hexUuid.trim()));
  /* tslint:disable:no-bitwise */
  uuidSeed[6] = (uuidSeed[6] & 0x0f) | 0x40;
  uuidSeed[8] = (uuidSeed[8] & 0x3f) | 0x80;
  /* tslint:enable:no-bitwise */
  return forge.util.bytesToHex(
    [...uuidSeed].map(byte => String.fromCharCode(byte)).join('')
  );
};
