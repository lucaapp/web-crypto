import {
  base32CrockfordToHex,
  bitArrayToUint8Array,
  hexToBase32Crockford,
  int32ToHex,
  int32ToUint8Array,
  uint8ArrayToBitArray,
  uint8ArrayToInt32,
} from './encoding';
import { Bit } from './types';

const int32HexUnit8Tuples = [
  [-2147483648, '00000080', [128, 0, 0, 0]],
  [2147483649, '01000080', [128, 0, 0, 1]],
  [0, '00000000', [0, 0, 0, 0]],
];

test('int32ToHex', () => {
  for (const [int32, hex] of int32HexUnit8Tuples) {
    expect(int32ToHex(int32 as number)).toBe(hex);
  }
});

test('int32ToUint8Array', () => {
  for (const [int32, , uint8values] of int32HexUnit8Tuples) {
    expect(int32ToUint8Array(int32 as number)).toEqual(
      new Uint8Array(uint8values as number[])
    );
  }
});

const uint32HexUnit8Tuples = [
  [0, '00000000', [0, 0, 0, 0]],
  [2147483649, '01000080', [128, 0, 0, 1]],
];

test('uint8ArrayToInt32', () => {
  for (const [uint32, , uint8values] of uint32HexUnit8Tuples) {
    expect(uint8ArrayToInt32(uint8values as number[])).toBe(uint32);
  }
});

const bitAndUint8Arrays = [
  [[0, 0, 0, 0, 0, 0, 0, 1], [1]],
  [[1, 0, 0, 0, 0, 0, 0, 0], [128]],
  [[1, 0, 0, 0, 0, 0, 0, 1], [129]],
  [
    [1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0],
    [129, 128],
  ],
];

test('bitArrayToUint8Array', () => {
  for (const [bitArray, uint8Array] of bitAndUint8Arrays) {
    expect(bitArrayToUint8Array(bitArray as Bit[])).toEqual(
      new Uint8Array(uint8Array)
    );
  }
});

test('uint8ArrayToBitArray', () => {
  for (const [bitArray, uint8Array] of bitAndUint8Arrays) {
    expect(uint8ArrayToBitArray(new Uint8Array(uint8Array))).toEqual(bitArray);
  }
});

const crockfordEncoded = 'DHTP6RBCENHP2V3NCDGPRXB3C4';
const hexEndoded = '6c7563616c7563616c7563616c756361';
test('base32ToHexCrockford compatibility to previous implementation', () => {
  expect(base32CrockfordToHex(crockfordEncoded)).toBe(hexEndoded);
});
test('hexToBase32Crockford compatibility to previous implementation', () => {
  expect(hexToBase32Crockford(hexEndoded)).toBe(crockfordEncoded);
});
