import {
  ECDH,
  KDF_SHA256,
  HMAC_SHA256,
  DECRYPT_DLIES,
  ENCRYPT_DLIES,
  ENCRYPT_AES_CTR,
  GET_RANDOM_BYTES,
  ECDH_VARIABLE_SIZE,
  EC_KEYPAIR_GENERATE,
} from './crypto';
import { Hex } from './types';
import { bytesToHex } from './encoding';

const TEST_IV = bytesToHex('Pqi8i7BPaVPpQXXg');
const TEST_DATA = bytesToHex('Hello World!');
const TEST_KEY_PAIR_31_BYTES_A = {
  privateKey:
    '0691ebe990367885c8a9df98d9cbaee84d0964ac9fdf69668a8939def0b85666',
  publicKey:
    '043dfcd9110127f5b6448991caf6efd58b7d7976eff6c6f5ffb7b013b6adddb2836432cb4f45749059c103ebc28bd0e7f03cccca3d7134012f72f3fa411015ff55',
};
const TEST_KEY_PAIR_31_BYTES_B = {
  privateKey:
    '2770d710fb4cb76552a2093323c2900016e2f4066d613e5105f7ba46af629bf9',
  publicKey:
    '045e9d34d52adf4a63fce9bebe95bfcd5698e3eaad5e2384446448fc86a06195d81fa4d5d3c39dd87e517ea89edf49d67d649c3d9627d2b3123d415218f63efeb7',
};

function ENCRYPT_DLIES_DETERMINISTIC(
  data: Hex,
  privateKey: Hex,
  publicKey: Hex,
  iv: Hex,
  ecdh: (privateKey: Hex, publicKey: Hex) => Hex
) {
  const dhKey = ecdh(privateKey, publicKey);
  const encryptionKey = KDF_SHA256(dhKey, '01').slice(0, 32);
  const authKey = KDF_SHA256(dhKey, '02');
  const encryptedData = ENCRYPT_AES_CTR(data, encryptionKey, iv);
  const mac = HMAC_SHA256(encryptedData, authKey);

  return {
    iv,
    mac,
    data: encryptedData,
  };
}

describe('crypto', () => {
  describe('ECDH', () => {
    it('should return 32 bytes key size even if result is 31 bytes', () => {
      const EXPECTED_ECDH_RESULT =
        '004d0c74e22e2500ec7d355a866d10671acf0b9a80fedce171f0b96cc9a497d2';

      const ecdhResultA = ECDH(
        TEST_KEY_PAIR_31_BYTES_A.privateKey,
        TEST_KEY_PAIR_31_BYTES_B.publicKey
      );
      const ecdhResultB = ECDH(
        TEST_KEY_PAIR_31_BYTES_B.privateKey,
        TEST_KEY_PAIR_31_BYTES_A.publicKey
      );

      expect(ecdhResultA).toEqual(EXPECTED_ECDH_RESULT);
      expect(ecdhResultB).toEqual(EXPECTED_ECDH_RESULT);
    });
  });
  describe('ECDH_VARIABLE_SIZE', () => {
    it('should return 31 bytes if the result is 31 bytes', () => {
      const EXPECTED_ECDH_VARIABLE_RESULT =
        '4d0c74e22e2500ec7d355a866d10671acf0b9a80fedce171f0b96cc9a497d2';

      const ecdhResultA = ECDH_VARIABLE_SIZE(
        TEST_KEY_PAIR_31_BYTES_A.privateKey,
        TEST_KEY_PAIR_31_BYTES_B.publicKey
      );
      const ecdhResultB = ECDH_VARIABLE_SIZE(
        TEST_KEY_PAIR_31_BYTES_B.privateKey,
        TEST_KEY_PAIR_31_BYTES_A.publicKey
      );

      expect(ecdhResultA).toEqual(EXPECTED_ECDH_VARIABLE_RESULT);
      expect(ecdhResultB).toEqual(EXPECTED_ECDH_VARIABLE_RESULT);
    });
  });
  describe('DECRYPT_DLIES', () => {
    it('should generally be able to decrypt data that was encrypted with ENCRYPT_DLIES', () => {
      const keyPair = EC_KEYPAIR_GENERATE();
      const cipher = ENCRYPT_DLIES(keyPair.publicKey, TEST_DATA);
      const result = DECRYPT_DLIES(
        keyPair.privateKey,
        cipher.publicKey,
        cipher.data,
        cipher.iv,
        cipher.mac
      );
      expect(result).toEqual(TEST_DATA);
    });
    it('should not be able to decrypt data that was encrypted with ENCRYPT_DLIES if the mac is invalid', () => {
      const keyPair = EC_KEYPAIR_GENERATE();
      const cipher = ENCRYPT_DLIES(keyPair.publicKey, TEST_DATA);
      const run = () =>
        DECRYPT_DLIES(
          keyPair.privateKey,
          cipher.publicKey,
          cipher.data,
          cipher.iv,
          GET_RANDOM_BYTES(16)
        );
      expect(run).toThrow('Invalid MAC');
    });
    it('should be able to decrypt data that was encrypted with ENCRYPT_DLIES using ECDH with 31 bytes ECDH key result', () => {
      const cipher = ENCRYPT_DLIES_DETERMINISTIC(
        TEST_DATA,
        TEST_KEY_PAIR_31_BYTES_B.privateKey,
        TEST_KEY_PAIR_31_BYTES_A.publicKey,
        TEST_IV,
        ECDH
      );

      const decrypted = DECRYPT_DLIES(
        TEST_KEY_PAIR_31_BYTES_B.privateKey,
        TEST_KEY_PAIR_31_BYTES_A.publicKey,
        cipher.data,
        cipher.iv,
        cipher.mac
      );

      expect(decrypted).toEqual(TEST_DATA);
    });
    it('should be able to decrypt data that was encrypted with ENCRYPT_DLIES using ECDH_VARIABLE_SIZE with 31 bytes ECDH key result', () => {
      const cipher = ENCRYPT_DLIES_DETERMINISTIC(
        TEST_DATA,
        TEST_KEY_PAIR_31_BYTES_A.privateKey,
        TEST_KEY_PAIR_31_BYTES_B.publicKey,
        TEST_IV,
        ECDH_VARIABLE_SIZE
      );
      const decrypted = DECRYPT_DLIES(
        TEST_KEY_PAIR_31_BYTES_B.privateKey,
        TEST_KEY_PAIR_31_BYTES_A.publicKey,
        cipher.data,
        cipher.iv,
        cipher.mac
      );

      expect(decrypted).toEqual(TEST_DATA);
    });
  });
});
