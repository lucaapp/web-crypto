import * as forge from 'node-forge';

export type Bit = 0 | 1;
export type Hex = string;
export type Z85 = string;
export type Bytes = string;
export type Base32 = string;
export type Base64 = string;
export type Base85 = string;

export type PEM = string;
export type CAStore = forge.pki.CAStore;
export type Certificate = forge.pki.Certificate;
export type RSAKeyPair = forge.pki.rsa.KeyPair;

export type ECKeyPair = {
  privateKey: Hex;
  publicKey: Hex;
  compressedPublicKey: Hex;
};

export type Issuer = {
  issuerId: string;
  publicCertificate: PEM;
  signedPublicHDSKP: string;
  signedPublicHDEKP?: string;
};

export type BaseIssuer = {
  issuerId: string;
  name: string;
  publicCertificate: PEM;
};
