module.exports = {
  roots: ['<rootDir>/src'],
  preset: 'ts-jest',
  testEnvironment: 'node',
  collectCoverage: true,
  verbose: true,
  coveragePathIgnorePatterns: ['/node_modules/', 'testHelper'],
};
