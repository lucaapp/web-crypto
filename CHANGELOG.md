# Changelog

### 4.2.1
* Move Elliptic curve too EllipticCurve singleton

### 4.2.0
* Add jwt creation/verification functions for publicBadgeKeys, encryptedPrivateBadgeKeys and HDEKP
* Add expiry attribute to key jwts

### 4.1.0
* get organizational name from x509
 
### 4.0.2
* update dependencies

### 4.0.1
* update dependencies

### 4.0.0
* Add option to overwrite issue time
* Optimized types
* Renamed uuid to issuerId

### 3.0.2
* Add export for encryptedPrivateDailyKey entities file

### 3.0.1
* Fix certificate chain order

### 3.0.0
* Add sonar to jenkins ci
* Enable test coverage
* Add jwt verifiers
* Move pem functions to x509.ts
* Change x509 functions to work with pem files

### 2.1.2
* Update dependencies
* Add Jenkinsfile
* Add Sonarcube config

### 2.1.1
* Fix assert package issue

### 2.1.0
* Add pem file helper functions
* Add verifyLocationTransfer function

### 2.0.4
* Add publicKeyToECPublicKeyPEM function
* Add privateKeyToECPrivateKeyPEM function

### 2.0.3
* Fix npm release issue

### 2.0.2 (2021-06-24)
* Added try catch handling to verify functions

### 2.0.1 (2021-05-31)
* Fix condition for mac check

### 2.0.0 (2021-05-31)
* Add GCM Tag check
* Add comments to crypto.ts
* Change ECDH to output fixed size results
* Support fixed and variable sized encryption DECRYPT_DLIES


### 1.0.3 (2021-04-14)

* initial public release
