# Luca Web Crypto

[luca](https://luca-app.de) ensures a data protection-compliant, decentralized
encryption of your data, undertakes the obligation to record contact data for
events and gastronomy, relieves the health authorities through digital, lean,
and integrated processes to enable efficient and complete tracing.

This service contains the source code of the luca web crypto package.
The Luca Web Crypto package contains all crypto functionalities which are needed by our different web services. (You can find our webservices [here](https://gitlab.com/lucaapp/web/)).

## Changelog

An overview of all releases can be found
[here](https://gitlab.com/lucaapp/web-crypto/-/blob/master/CHANGELOG.md).

## Issues & Support

Please [create an issue](https://gitlab.com/lucaapp/web-crypto/-/issues) for
suggestions or problems related to this application. For general questions,
please check out our [FAQ](https://www.luca-app.de/faq/) or contact our support
team at [hello@luca-app.de](mailto:hello@luca-app.de).

## License

The luca web crypto package is Free Software (Open Source) and is distributed
with a number of components with compatible licenses.

```
SPDX-License-Identifier: Apache-2.0

SPDX-FileCopyrightText: 2021 culture4life GmbH <https://luca-app.de>
```

For details see
* [license file](https://gitlab.com/lucaapp/web-crypto/-/blob/master/README.md)
