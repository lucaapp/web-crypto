#!/bin/sh
set -o nounset

YARN_PUBLIC_REGISTRY=https://registry.yarnpkg.com/

# replace npm registry references
sed -i -e "s+$YARN_PUBLIC_REGISTRY+$NPM_REGISTRY+g" "yarn.lock"

# change npm and yarn options for compatibility with private registry
cat <<EOF >> ".npmrc"
always-auth=true
registry=${NPM_REGISTRY}
strict-ssl=false
EOF
  cat <<EOF >> ".yarnrc"
strict-ssl false
EOF
